/**
* Author: Pol Layola Izquierdo
* Date: March 2018
*
* pressure-sensor.c
*   Functionalities to Setup and Read a digital pin from CC2538.
*   @function   sensor_init_pressure  @return void
*   @function   sensor_get_pressure   @return unsigned char
*/

#include <stdio.h>
#include "contiki.h"
#include "gpio.h"
#include "dev/pressure-sensor.h"

#define SENSOR_GPIO  GPIO_C_BASE  /* Port Base C */
#define SENSOR_MASK  0x01         /* GPIO pin 0 */

/*-------------------------------------------------------*/
/**
* Initialize pressure GPIO
*/
void sensor_init_pressure(void)
{
  GPIO_SET_INPUT(PRESSURE_GPIO, PRESSURE_MASK);
}
/*-------------------------------------------------------*/
/**
* Read pressure from a GPIO
*/
unsigned char sensor_get_pressure(void)
{
  char pressure = 0;
  return pressure = GPIO_READ_PIN(PRESSURE_GPIO, PRESSURE_MASK);
}
/*-------------------------------------------------------*/
