/**
* Author: Pol Layola Izquierdo
* Date: March 2018
*
* pressure-sensor-demo.c
*   Setup and Read a digital pin from CC2538 using an external library.
*/

#include <stdio.h>
#include "contiki.h"
#include "pressure-sensor.h"

/*-------------------------------------------------------*/
PROCESS(pressure_sensor_demo_process, "Pressure sensor demo process");
AUTOSTART_PROCESSES(&pressure_sensor_demo_process);
/*-------------------------------------------------------*/
static struct etimer et;
/*-------------------------------------------------------*/
PROCESS_THREAD(pressure_sensor_demo_process, ev, data) {

  PROCESS_BEGIN();

  sensor_init_pressure();

  while(1) {
    unsigned char pressure = 0;

    etimer_set(&et, CLOCK_SECOND);
    PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));

    pressure = sensor_get_pressure();

    printf("Pressure: %u \n", pressure);
  }

  PROCESS_END();
};
/*-------------------------------------------------------*/
