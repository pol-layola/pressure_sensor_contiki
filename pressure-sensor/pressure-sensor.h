/**
* Author: Pol Layola Izquierdo
* Date: March 2018
*
* pressure-sensor.h
*   Library with functionalities to Setup and Read a digital pin from CC2538.
*   @function   sensor_init_pressure  @return void
*   @function   sensor_get_pressure   @return unsigned char
*/

#ifndef PRESSURE_SENSOR
#define PRESSURE_SENSOR

void sensor_init_pressure(void);
unsigned char sensor_get_pressure(void);

#endif /* PRESSURE_SENSOR */
