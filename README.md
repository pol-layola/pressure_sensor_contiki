# Intelligent Chair #

## Pressure Sensor - Contiki Project

### Start here: ###
```

1. git clone https://pol-layola@bitbucket.org/pol-layola/pressure_sensor_contiki.git
2. cd pressure_sensor_contiki

```

## Folders:

### Pressure sensor ###
Setup and Read a digital pin from CC2538 using Contiki and external libraries.

### Sleep Mode Test ###
*** Developing ***
Setup and Read a digital pin from CC2538 with interrupts and sleep mode management.
External library with setup, read, interrupts and sleep mode functionalities for CC2538.
