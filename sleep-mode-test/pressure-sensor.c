#include <stdio.h>
#include "contiki.h"
#include "gpio.h"
#include "dev/pressure-sensor.h"

#define SENSOR_GPIO  GPIO_C_BASE  /* Port Base C */
#define SENSOR_MASK  0x01         /* GPIO pin 0 */

/*-------------------------------------------------------*/
/* Initialize pressure GPIO as input */
void sensor_init_pressure(void) {
  GPIO_SET_INPUT(PRESSURE_GPIO, PRESSURE_MASK);
}
/*-------------------------------------------------------*/
/* Read pressure from a GPIO */
unsigned char sensor_get_pressure(void) {
  char pressure = 0;
  return pressure = GPIO_READ_PIN(PRESSURE_GPIO, PRESSURE_MASK);
}
/*-------------------------------------------------------*/
/* Configure and initialize sleep mode */
void sensor_init_sleep_mode(void) {
  printf("Initialize Default Sleep Mode\n");
  // Default Sleep Mode ????
/*
  // Initialize Low Power Mode
  lpm_init();
  // Enter sleep mode
  lpm_sleep();
  // Set power mode
  // SYS_CTRL_PM_NOACTION, SYS_CTRL_PM_1, SYS_CTRL_PM_2 or SYS_CTRL_PM_3
  SysCtrlPowerModeSet(uint32_t ui32PowerMode);
*/
}
/*-------------------------------------------------------*/
/* Enable sleep mode */
void sensor_enable_sleep_mode(void) {
  printf("Enabling Sleep Mode\n");
}
/*-------------------------------------------------------*/
/* Disable sleep mode */
void sensor_disable_sleep_mode(void) {
  printf("Disable Sleep Mode\n");
}
/*-------------------------------------------------------*/
/* Configure and initialize wake up interrupt */
void sensor_init_interrupts(void) {
  printf("Configure and initialize interrupts\n");
  /*
  // Enable wake up interrupt - SYS_CTRL_IWE register
  ????
  // Power-up interrupt type
  GPIOPowIntTypeSet(PRESSURE_GPIO, PRESSURE_MASK, GPIO_POW_RISING_EDGE);
  // Clear pending GPIO power-up interrupts
  GPIOPowIntClear(PRESSURE_GPIO, PRESSURE_MASK);
  // Enable power-up interrupt
  GPIOPowIntEnable(PRESSURE_GPIO, PRESSURE_MASK);
  */
  // Enable interrupt triggering --> ¿¿¿ No need ???
  printf("Enable interrupt\n");
  GPIO_ENABLE_INTERRUPT(PRESSURE_GPIO, PRESSURE_MASK);
  // Triggering type: falling edge or rising edge
  printf("Set power up on falling edge\n");
  GPIO_POWER_UP_ON_FALLING(PRESSURE_GPIO, PRESSURE_MASK);
  // Enable power-up interrupt triggering
  printf("Enable power up interrupt\n");
  GPIO_ENABLE_POWER_UP_INTERRUPT(PRESSURE_GPIO, PRESSURE_MASK);
}
/*-------------------------------------------------------*/
