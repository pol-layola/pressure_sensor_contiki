/**
* pressure_sensor.h
*   @function   sensor_init_pressure  @return void
*   @function   sensor_get_pressure   @return unsigned char
*/
#ifndef PRESSURE_SENSOR
#define PRESSURE_SENSOR

void sensor_init_pressure(void);
unsigned char sensor_get_pressure(void);

void sensor_init_sleep_mode(void);
void sensor_enable_sleep_mode(void);
void sensor_disable_sleep_mode(void);
void sensor_init_interrupts(void);

#endif /* PRESSURE_SENSOR */
