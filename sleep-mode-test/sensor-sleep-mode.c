/*
1. Set up GPIO pin(s) as input (if a GPIO port is used as power-up interrupt source) using GPIODirModeSet().
2. Enable wake up interrupt for the desired port using the SYS_CTRL_IWE register.
3. Set the power-up interrupt type for the specified pin(s), using GPIOPowIntTypeSet().
4. Clear any pending GPIO power-up interrupts for the specified pin(s) using GPIOPowIntClear().
5. Enable power-up interrupt for the specified port, using GPIOPowIntEnable().
6. The device can now be put to sleep (all power modes) and will wake up on the configured
interrupt.
*/

/* 1. GPIO as input */
GPIODirModeSet(uint32_t ui32Port, uint8_t ui8Pins, uint32_t ui32PinIO);
GPIO_SET_INPUT(PRESSURE_GPIO, PRESSURE_MASK);

/* 2. Enable wake up interrupt - SYS_CTRL_IWE register */
????

/* 3. Power-up interrupt type */
/* Types: GPIO_POW_FALLING_EDGE / GPIO_POW_RISING_EDGE */
GPIOPowIntTypeSet(uint32_t ui32Port, uint8_t ui8Pins, uint32_t ui32IntType);
GPIOPowIntTypeSet(PRESSURE_GPIO, PRESSURE_MASK, GPIO_POW_RISING_EDGE);

/* 4. Clear pending GPIO power-up interrupts */
GPIOPowIntClear(uint32_t ui32Port, uint8_t ui8Pins);
GPIOPowIntClear(PRESSURE_GPIO, PRESSURE_MASK);

/* 5. Enable power-up interrupt */
GPIOPowIntEnable(uint32_t ui32Port, uint8_t ui8Pins);
GPIOPowIntEnable(PRESSURE_GPIO, PRESSURE_MASK);

/* 6. Enable sleep mode */
????

/* Questions:
*** Difference between wake up interrupt and power-up interrupt?
*** How to modify SYS_CTRL_IWE register?
*/
